import Vue from "vue";
import Vuex from "vuex";
// import * as modules from "./modules";
import modules from "./modules";
import data from "./data";
import api from "./api";
import pathify from "vuex-pathify";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    ...modules,
    ...data,
    ...api
  },
  plugins: [pathify.plugin]
});
