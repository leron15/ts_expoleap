// Utilities
import { make, Payload } from "vuex-pathify";

const DEFAULT_RIGHTNAVDATA = Object.freeze({
  color: "primary",
  href: false,
  msg: "",
  text: "Close",
  to: false,
  timeout: 6000,
  action: () => {}
});

export const state = {
  currentVersion: process.env.VUE_APP_VERSION,
  drawer: null,
  rightDrawer: null,
  isLoading: false,
  releases: [],
  rightNavData: {
    ...DEFAULT_RIGHTNAVDATA
  },
  eventDialog: true as boolean
  //   supporters: require('@/data/api/supporters.json'),
};

export const mutations = {
  ...make.mutations(state),

  SET_EVENT_DIALOG: (state: any, payload: boolean) => {
    state.eventDialog = payload;
  },
  setRightNavData: (state: any, payload: object) => {
    state.rightNavData = Object.assign(
      {},
      {
        color: "primary",
        href: false,
        msg: "",
        text: "Close",
        to: false,
        timeout: 6000,
        action: () => {}
      },
      payload
    );
  }
};

export const actions = {};

export const getters = {};

// process.env.VUE_APP_VERSION = require('./package.json').version
// process.env.VUE_APP_API_URL = 'http://alserver.ddns.net:8000' // "http://localhost:8000/api/" //Use for production
