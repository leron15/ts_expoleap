// Utilities
import camelCase from "lodash/camelCase";
import upperFirst from "lodash/upperFirst";
import { make } from "vuex-pathify";

function addHeadingAndAd(children: any[]) {
  console.log("children", children);

  children.splice(0, 0, {
    type: "section",
    children: [
      { type: "heading", lang: "heading" },
      { type: "base-text", lang: "headingText" }
      // { type: "ad-entry" }
    ]
  });
}

function getHeadings(children: any, toc = []) {
  for (const child of children) {
    if (child.children) {
      getHeadings(child.children, toc);

      continue;
    }

    if (
      ![
        "detail",
        "accessibility",
        "api",
        "examples",
        "heading",
        "up-next",
        "usage-new",
        "variable-api"
      ].includes(child.type)
    )
      continue;

    if (child.type === "heading") {
      toc.push(child.lang);
    } else {
      toc.push(`Generic.Pages.${camelCase(child.type)}`);
    }
  }

  return toc;
}

function getNamespace(namespace: string, id?: string): string {
  switch (namespace) {
    case "events":
      return `events/all`;
    case "calendar":
      return `calendar/all`;
    case "sponsors":
      return `sponsors/all`;
    case "speakers":
      return `speakers/all`;
    case "news":
      return `news/all`;
    default:
      return "";
  }
}

function addFooterAd(children = []) {
  if (!children.length) return;

  const index = children.length - 1;
  const childChildren = children[index].children || [];

  // childChildren.push({ type: "ad-exit" });
  children[index].children = childChildren;
}

export const state = {
  deprecatedIn: require("@/data/deprecated.json"),
  links: require("@/data/drawerItems.json"),
  newIn: require("@/data/new.json"),
  namespace: null,
  page: null,
  id: null,
  structure: null,
  templates: require("@/data/templates.json"),
  toc: []
};

export const mutations = make.mutations(state);

export const actions = {};

export const getters = {
  breadcrumbs(state: any, getters: any, rootState: any) {
    if (!rootState.route) return [];

    const namespace = rootState.route.params.namespace;
    const lang = rootState.route.params.lang;
    const page = rootState.route.params.page;
    const id = rootState.route.params.id;
    const path = rootState.route.path;
    const text = getNamespace(namespace, id);

    const final = [
      {
        text: upperFirst(namespace),
        to: text ? `/${lang}/${text}` : undefined,
        disabled: !text,
        exact: false
      }
    ];
    id
      ? final.push({
          text: rootState.route.params.id,
          to: path,
          disabled: false,
          exact: true
        })
      : undefined,
      page
        ? final.push({
            text: upperFirst(rootState.route.params.page),
            to: path,
            disabled: !text,
            exact: false
          })
        : undefined;

    return [...final];
  },
  headings(state, getters) {
    return getHeadings(getters.structure);
  },
  namespace(state, getters, rootState) {
    return !rootState.route
      ? undefined
      : upperFirst(camelCase(rootState.route.params.namespace));
  },
  page(state, getters, rootState) {
    return !rootState.route
      ? undefined
      : upperFirst(camelCase(rootState.route.params.page));
  },
  id(state, getters, rootState) {
    return !rootState.route
      ? undefined
      : upperFirst(camelCase(rootState.route.params.id));
  },
  structure(state: any, getters: any, rootState: any) {
    const children = JSON.parse(
      JSON.stringify((state.structure || {}).children || [])
    );

    if (!children.length) return children;

    // addHeadingAndAd(children);
    // addFooterAd(children);

    return children;
  },
  api_name(state: any, getters: any, rootState: any) {
    const api = JSON.parse(JSON.stringify((state.structure || {}).api || ""));

    if (!api) return api;

    // addHeadingAndAd(children);
    // addFooterAd(children);

    return api as Array<String> | string;
  },
  themes(state) {
    return Object.values(state.templates);
  }
};

export default {
  namespaced: true,
  ...state,
  ...mutations,
  ...actions,
  ...getters
};
