// Utilities
import { set } from "../utils";

const DEFAULT_SNACKBAR = Object.freeze({
  color: "primary",
  href: false,
  msg: "",
  text: "Close",
  to: false,
  timeout: 6000,
  action: () => {}
});

export const state = {
  snackbar: {
    ...DEFAULT_SNACKBAR
  },
  value: false
};

export const mutations = {
  setSnackbar: (state: any, payload: object) => {
    state.snackbar = Object.assign(
      {},
      {
        color: "primary",
        href: false,
        msg: "",
        text: "Close",
        to: false,
        timeout: 6000,
        action: () => {}
      },
      payload
    );
  },
  setValue: set("value")
};

export const actions = {};

export const getters = {};
