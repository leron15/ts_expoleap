// declare namespace Auth {
// declare module "@altoleap/auth" {
export interface User extends AccountInfo {
  id: number | string;
  first_name: string | null;
  last_name: string | null;
  email: string | null;
  // password: string;
  events: any[] | null;
  avatar: string | null;
}
export interface AccountInfo {
  id: number | string;
  first_name: string | null;
  last_name: string | null;
  email: string | null;
  // password: string;
  events: any[] | null;
  avatar: string | null;
}
// }

export class AltoleapAuth {
  public constructor();

  currentUser: User | null;
}

interface NameServiceMapping {
  // eslint-disable-next-line prettier/prettier
    'auth': AltoleapAuth;
}
// }
