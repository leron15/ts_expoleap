export const set = (property: string | number) => (store: any, payload: any) =>
  (store[property] = payload);
export const toggle = (property: string | number) => (store: any) =>
  (store[property] = !store[property]);
export const push = (property: string | number) => (store: any, payload: any) =>
  store[property].push(payload);
