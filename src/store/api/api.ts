import { make } from "vuex-pathify";

export const state = {
  API: undefined as any
};

export const mutations = make.mutations(state);

export const actions = {};

export const getters = make.getters(state);
