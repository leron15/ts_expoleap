import { make } from "vuex-pathify";
// import * as auth from '@/store/models/@altoleap/auth';
import { AccountInfo, AltoleapAuth } from "@/store/models/auth";
// /// <reference path="@/store/models/auth.d.ts"/>
// import { NameServiceMapping, AccountInfo, AltoleapAuth } from "@altoleap/auth";

export const state = {
  user: {
    id: "",
    first_name: null,
    last_name: null,
    email: null,
    // password: null,
    events: null,
    avatar: null
  } as AccountInfo
};

export const mutations = make.mutations(state);

export const actions = {};

export const getters = {
  getuser(state: any, getters: any, rootState: any) {
    return state.user;
  }
};
