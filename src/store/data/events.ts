import { make } from "vuex-pathify";

export const state = {
  list: [],
  instance: {}
};

export const mutations = make.mutations(state);

export const actions = {};

export const getters = {
  GET_EVENTS_LIST(state: any, getters: any, rootState: any) {
    return state.list;
  },
  GET_CURRENT_EVENT(state: any, getters: any, rootState: any) {
    return state.instance;
  }
};
