import Vue from "vue";
import "./plugins/axios";
import App from "./App.vue";
import "./registerServiceWorker";

// Bootstrap
import vuetify from "./plugins";
import "@/components";
// import { createI18n } from '@/i18n/index';

import router from "./router";
import store from "./store";
import { sync } from "vuex-router-sync";

Vue.config.productionTip = false;
Vue.config.performance = process.env.NODE_ENV === "development";

// const i18n = createI18n(null, router);

sync(store, router);

new Vue({
  router,
  store,
  // i18n,
  vuetify,
  render: h => h(App),
  created() {
    // eslint-disable-next-line no-console
    console.log("APP VERSION", process.env.VUE_APP_VERSION);
    console.log("API URL", process.env.VUE_APP_API_URL);
  }
}).$mount("#app");
