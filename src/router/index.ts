// Vue
import Vue from "vue";
import VueRouter from "vue-router";
import scrollBehavior from "./scroll-behavior";

// Settings
import redirects from "./301.json";

// Utilities
import { layout, redirectLang, root, route } from "./util";

import { JwtHelper } from "@/services/token";

Vue.use(VueRouter);
const authMeta = {
  requiresAuth: false,
  public: true, // Allow access to even if not logged in
  onlyWhenLoggedOut: true
};

const routes = root([
  layout("", "Frontend", [
    ...Object.keys(redirects).map(k => ({
      path: k.replace(/^\//, ""),
      redirect: () => redirects[k].replace(/^\//, "")
    }))
  ]),
  layout("", "Auth", [
    route("auth/:page?", "Auth", authMeta),
    route("select/", "Select", {
      requiresAuth: true,
      public: false,
      onlyWhenLoggedOut: false
    })
  ]),
  layout("", "App", [
    route("/", "Home", {
      requiresAuth: true,
      public: false,
      onlyWhenLoggedOut: false
    }),
    // route(
    //   ":namespace/:id([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})?/:page?",
    //   "Page"
    // )
    route(":namespace/:id?/:page?", "Page", {
      requiresAuth: true,
      public: false,
      onlyWhenLoggedOut: false
    })
    // redirectLang('/404', 'en'),
  ])
  // redirectLang(''),
]);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior
});
const token = new JwtHelper();
router.beforeEach((to, from, next) => {
  console.log(Object.keys(redirects));

  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(
    record => record.meta.onlyWhenLoggedOut
  );
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  const loggedIn = !!token.getToken;
  console.log(requiresAuth && !loggedIn && !isPublic);

  // const expiredToken = TokenService.isTokenExpired()
  // const eventID = EventService.getEventId()

  if (requiresAuth && !loggedIn && !isPublic) {
    return next({
      name: "Auth"
      // query: { redirect: to.fullPath }
      // Store the full path to redirect the user to after login
    });
  }
  // if (loggedIn && onlyWhenLoggedOut) {
  //   return next({
  //     name: "Home",
  //     // params: { eventid: "eventID" }
  //   });
  // }

  next();
});

export default router;
