import goTo from "vuetify/src/services/goto";
import { waitForReadystate } from "@/utils/componentHelpers";

export default async function(
  to: any,
  from: any,
  savedPosition: any
): Promise<any> {
  await waitForReadystate();

  const options = {};
  let scrollTo: any = 0;

  if (to.hash) {
    scrollTo = to.hash;
  } else if (savedPosition) {
    scrollTo = savedPosition.y;
  }

  return new Promise(resolve => {
    setTimeout(() => {
      if (typeof window === "undefined") {
        return resolve();
      }

      window.requestAnimationFrame(() => {
        try {
          goTo(scrollTo, options);
        } catch (err) {
          console.log(err);
        }

        resolve();
      });
    }, 200);
  });
}
