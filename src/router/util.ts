import kebabCase from "lodash/kebabCase";
import languages from "@/data/i18n/languages.json";

interface Meta {
  requiresAuth?: boolean;
  public?: boolean;
  onlyWhenLoggedOut?: boolean;
}

export function getLanguageCookie() {
  return typeof document === "undefined"
    ? undefined
    : new Map(document.cookie.split("; ").map(c => c.split("="))).get(
        "currentLanguage"
      );
}

export function root(children: any[]) {
  return [
    layout(
      "/:lang([a-z]{2,3}|[a-z]{2,3}-[a-zA-Z]{4}|[a-z]{2,3}-[A-Z]{2,3})",
      // "",
      "root",
      children
    )
  ];
}

export function layout(path: string, name: string, children?: any[]) {
  const folder = kebabCase(name);

  return {
    path,
    component: () => import(`@/layouts/${folder}/Index.vue`),
    props: true,
    children
  };
}

export function route(
  path: string,
  name: string,
  meta?: Meta,
  children?: any[],
  file?: string
) {
  const folder = (file || `${kebabCase(name)}`).toLowerCase();

  return {
    component: () => import(`@/views/${folder}/Index.vue`),
    name,
    path,
    meta,
    children
  };
}
export function redirect(redirect: () => string) {
  return { path: "*", redirect };
}

export function redirectLang(path: string, locale: string) {
  // language regex:
  // /^[a-z]{2,3}(?:-[a-zA-Z]{4})?(?:-[A-Z]{2,3})?$/
  // /^[a-z]{2,3}|[a-z]{2,3}-[a-zA-Z]{4}|[a-z]{2,3}-[A-Z]{2,3}$/
  const languageRegex = /^\/([a-z]{2,3}|[a-z]{2,3}-[a-zA-Z]{4}|[a-z]{2,3}-[A-Z]{2,3})(?:\/.*)?$/;
  // const fallbackLocale = languages.find((lang:any) => lang.fallback === true).locale;

  return redirect(() => {
    let lang = `/${getLanguageCookie() || locale}`;

    if (!languageRegex.test(lang)) lang = `/${locale}`;

    return `${lang}${path}`;
  });
}
