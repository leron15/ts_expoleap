export async function loadApi(store: any, apiKeys: string) {
  console.log("loadApi callback");
  const setAPI = async (api?: boolean | null) =>
    await store.commit("api/SET_API", api); //mutations
  setAPI(null);
  try {
    const API_MODULES = (await import("@/services/index")).modules;
    const moduleKeys = Object.keys(API_MODULES) as string[];
    if (moduleKeys.includes(apiKeys)) {
      const service = new API_MODULES[apiKeys].service();
      setAPI(service);
    } else {
      console.log("Not in array of keys", [apiKeys], "in", moduleKeys);
      setAPI(false);
    }
  } catch (err) {
    console.warn("ERROR_LOADING_API: \t", err);
  }
}
