// import { $_events } from "./events";

import camelCase from "lodash/camelCase";

const modules: any = {};
const requireModule = require.context("@/services", true, /\.ts$/);
// Dont regiter this file as a vuex module index.ts etc
requireModule.keys().forEach(fileName => {
  if (fileName.split("/")[1] === "token.ts") return;
  if (fileName.split("/")[1] === "index.ts") return;
  if (fileName.split("/")[1] === "error.ts") return;
  if (fileName.split("/")[2] === "interface.ts") return;
  if (fileName.split("/")[2] === "index.ts") return;

  const moduleName = camelCase(fileName.replace(/(\.\/|\Service.ts)/g, ""));
  modules[moduleName] = {
    ...requireModule(fileName)
  };
});

export { modules };
