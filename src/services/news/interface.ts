export interface News {
  id?: number | null;
  author?: string | null;
  event?: string | null;
  created?: Date | null;
  modified?: Date | null;
  pub_date?: Date | null;
  title?: string | null;
  photo?: string | null;
  content?: string | null;
}

export const NewsPrototype: News = {
  photo: "w",
  title: "w",
  id: null,
  author: "w",
  event: "w",
  created: null,
  modified: null,
  pub_date: null,
  content: "w"
};

// export const keysOfNews = keys<News>();
// console.log(NewsKeys);

// interface Props {
//   id: string;
//   name: string;
//   age: number;
// }
// // const keysOfProps = keys<Props>();

// console.log("JEYSSS", keysOfNews);
