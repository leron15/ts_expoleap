import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { News, NewsPrototype } from "./interface";
import { JwtHelper } from "@/services/token";
import { formData } from "@/utils/formData";


export { NewsService as service };
const token = new JwtHelper();
class NewsService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/news`;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.getToken}`;
    console.log("Initialized News Service");
  };

  apiKeys: any = Object.keys(NewsPrototype);
  // setEvent = async (id: string) => localStorage.setItem("news", id);
  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an news instance.
   *  @param {News} news
   */
  async create(data: News): Promise<AxiosResponse | News> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as News);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a news instance.
   */
  async fetch(): Promise<AxiosResponse[] | News[]> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a calendar instance.
   */
  async read(id: string): Promise<AxiosResponse | News> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}`)
        .then(response => {
          return resolve(response.data as News | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an eevnt instance.
   *  @param {News} news
   */
  async update(data: News): Promise<News | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${data.id}/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as News);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a calendar instance.
   *  @param {String} eventId
   */
  async delete(eventId: String): Promise<News | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .delete(`/${eventId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }

  async options(eventId: String): Promise<AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .options(`/${eventId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
