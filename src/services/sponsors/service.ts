import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { Sponsors, SponsorsPrototype } from "./interface";
import { JwtHelper } from "@/services/token";
import { formData } from "@/utils/formData";

export { SponsorsService as service };
const token = new JwtHelper();
class SponsorsService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/sponsors`;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.getToken}`;
    console.log("Initialized Sponsors Service");
  };

  apiKeys: any = Object.keys(SponsorsPrototype);
  // setEvent = async (id: string) => localStorage.setItem("data", id);
  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an Sponsors instance.
   *  @param {Sponsors} data
   */
  async create(data: Sponsors): Promise<AxiosResponse | Sponsors> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as Sponsors);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a data instance.
   */
  async fetch(): Promise<AxiosResponse[] | Sponsors[]> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a sponsor instance.
   */
  async read(id: string): Promise<AxiosResponse | Sponsors> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}`)
        .then(response => {
          return resolve(response.data as Sponsors | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an sponsor instance.
   *  @param {Sponsors} data
   */
  async update(data: Sponsors): Promise<Sponsors | AxiosResponse> {
    console.log("res", formData.setForm(data));
    // let payload = formData.setForm(data)
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${data.id}/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as Sponsors);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a sponsor instance.
   *  @param {String} instanceId
   */
  async delete(instanceId: String): Promise<Sponsors | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .delete(`/${instanceId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }

  async options(instanceId: String): Promise<AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .options(`/${instanceId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
