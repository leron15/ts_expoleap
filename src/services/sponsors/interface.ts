export interface Sponsors {
  id?: number | string;
  event?: string;
  name?: string;
  url?: string;
  logo?: string;
  phone?: string;
  email?: string;
  facebook?: string;
  twitter?: string;
  linkedin?: string;
  instagram?: string;
  description?: string;
  category?: number | string;
  category_name?: string;
}

export const SponsorsPrototype: Sponsors = {
  logo: "",
  id: "",
  event: "",
  name: "",
  url: "",
  phone: "",
  email: "",
  facebook: "",
  twitter: "",
  linkedin: "",
  instagram: "",
  description: "",
  category: "",
  category_name: ""
};

// export const keysOfNews = keys<News>();
// console.log(NewsKeys);

// interface Props {
//   id: string;
//   name: string;
//   age: number;
// }
// // const keysOfProps = keys<Props>();

// console.log("JEYSSS", keysOfNews);
