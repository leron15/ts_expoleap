export interface Tasks {
  id?: string | number;
  name?: string;
  description?: string;
  created?: string;
  due_date?: string;
  status?: string;
  order?: string | number;
}

export const TasksPrototype: Tasks = {
  id: "",
  name: "",
  description: "",
  created: "",
  due_date: "",
  status: "",
  order: ""
};
