import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { Tasks, TasksPrototype } from "./interface";
import { JwtHelper } from "@/services/token";

export { TasksService as service };
const token = new JwtHelper();
class TasksService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/tasks`;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.getToken}`;
    console.log("Initialized Tasks Service");
  };

  apiKeys: any = Object.keys(TasksPrototype);
  // setEvent = async (id: string) => localStorage.setItem("data", id);
  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an Tasks instance.
   *  @param {Tasks} data
   */
  async create(data: Tasks): Promise<AxiosResponse | Tasks> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, { ...data })
        .then(response => {
          resolve(response.data as Tasks);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a data instance.
   */
  async fetch(): Promise<AxiosResponse[] | Tasks[]> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a sponsor instance.
   */
  async read(id: string): Promise<AxiosResponse | Tasks> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}`)
        .then(response => {
          return resolve(response.data as Tasks | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an sponsor instance.
   *  @param {Tasks} data
   */
  async update(data: Tasks): Promise<Tasks | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${data.id}/`, { ...data })
        .then(response => {
          // console.log('Success');
          
          resolve(response.data as Tasks);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a sponsor instance.
   *  @param {String} instanceId
   */
  async delete(instanceId: String): Promise<Tasks | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .delete(`/${instanceId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }

  async options(instanceId: String): Promise<AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .options(`/${instanceId}`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
