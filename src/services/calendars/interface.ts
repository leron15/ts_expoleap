export interface Calendar {
  id: number | string;
  event?: string;
  name?: string;
  description?: string;
  start?: Date | string;
  end?: Date | string;
  start_date?: Date | string;
  start_time?: string;
  end_time?: string;
  location?: string;
  url?: string;
}

export const CalendarsPrototype: Calendar = {
  id: "",
  event: "",
  name: "",
  description: "",
  start: "",
  end: "",
  start_date: "",
  start_time: "",
  end_time: "",
  location: "",
  url: ""
};
