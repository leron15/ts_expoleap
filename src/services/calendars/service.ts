import { axios } from "@/plugins/axios";
import { errorCatch } from "../error";
import { Calendar, CalendarsPrototype } from "./interface";

export { CalendarService as service };
class CalendarService {
  init: any = async () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/calendar/`;
    return console.log("Initialized Calendar Service");
  };

  apiKeys: any = Object.keys(CalendarsPrototype);

  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an event instance.
   *  @param {Calendar} instance
   */
  async create(instance: Calendar): Promise<Calendar | any> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, { ...instance })
        .then(response => {
          resolve(response.data as Calendar);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a event instance.
   *
   */
  async fetch(): Promise<Calendar[] | any[]> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(response => {
          return resolve(response.data as Calendar[] | any[]);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a calendar instance.
   *  @param {Number | String} instanceId
   */
  async read(id: string | number): Promise<Calendar | any> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}`)
        .then(response => {
          return resolve(response.data as Calendar | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an eevnt instance.
   *  @param {Calendar} instance
   */
  async update(instance: Calendar): Promise<Calendar | any> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${instance.id}`, { ...instance })
        .then(response => {
          resolve(response.data as Calendar);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a calendar instance.
   *  @param {String} instanceId
   */
  async delete(instanceId: String): Promise<Calendar | any> {
    return await new Promise((resolve, reject) => {
      axios
        .delete(`/${instanceId}`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
