export interface Accounts {
  id?: number;
  first_name?: string;
  last_name?: string;
  email?: string;
  password?: string;
  events?: any[];
  avatar?: string;
}
