import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { JwtHelper } from "../token";
import { Accounts } from "./interface";

export { AuthService as service };
const decoder = new JwtHelper();
class AuthService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/auth`;
    return console.log("Initialized Auth Service");
  };
  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an event instance.
   *  @param {String} email
   *  @param {String} password
   *  @return {AxiosResponse}
   */
  async login(email: string, password: string): Promise<AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .post(
          `/`,
          { email: email, password: password }
          // { withCredentials: true }
        )
        .then(response => {
          if (response.status === 200) {
            decoder.setToken(response.data.token as string);
            response.data.user = decoder.decodeToken(response.data.token);
            return resolve(response.data);
          } else {
            reject("Cannot establish connection to server");
          }
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a event instance.
   */
  async fetch(): Promise<AxiosResponse> {
    console.log();

    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(
          (response): AxiosResponse => {
            return resolve(response.data);
          }
        )
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a calendar instance.
   */
  async read(id: string): Promise<Accounts | any> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}`)
        .then(response => {
          return resolve(response.data as Accounts | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an eevnt instance.
   *  @param {Accounts} event
   */
  async update(event: Accounts): Promise<Accounts | any> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${event.id}`, { ...event })
        .then(response => {
          resolve(response.data as Accounts);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a calendar instance.
   *  @param {String} eventId
   */
  async delete(eventId: String): Promise<Accounts | any> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${eventId}`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
