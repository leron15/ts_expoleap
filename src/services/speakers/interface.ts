export interface Speakers {
  id?: number | string;
  event?: string;
  first_name?: string;
  last_name?: string;
  job_title?: string;
  company?: string;
  email?: string;
  phone?: string;
  photo?: string;
  avatar?: string;
  label?: string;
  url?: string;
  facebook?: string;
  twitter?: string;
  linkedin?: string;
  bio?: string;
}

export const SpeakersPrototype: Speakers = {
  photo: "",
  avatar: "",
  id: "",
  event: "",
  first_name: "",
  last_name: "",
  job_title: "",
  company: "",
  email: "",
  phone: "",
  label: "",
  url: "",
  facebook: "",
  twitter: "",
  linkedin: "",
  bio: ""
};

// export const keysOfNews = keys<News>();
// console.log(NewsKeys);

// interface Props {
//   id: string;
//   name: string;
//   age: number;
// }
// // const keysOfProps = keys<Props>();

// console.log("JEYSSS", keysOfNews);
