import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { Speakers, SpeakersPrototype } from "./interface";
import { JwtHelper } from "@/services/token";
import { formData } from "@/utils/formData";


export { SpeakersService as service };
const token = new JwtHelper();
class SpeakersService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/speakers`;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.getToken}`;
    console.log("Initialized Speakers Service");
  };

  apiKeys: any = Object.keys(SpeakersPrototype);
  // setEvent = async (id: string) => localStorage.setItem("data", id);
  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an speakers instance.
   *  @param {Speakers} data
   */
  async create(data: Speakers): Promise<AxiosResponse | Speakers> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as Speakers);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a data instance.
   */
  async fetch(): Promise<AxiosResponse[] | Speakers[]> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a calendar instance.
   */
  async read(id: string): Promise<AxiosResponse | Speakers> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}/`)
        .then(response => {
          return resolve(response.data as Speakers | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an eevnt instance.
   *  @param {Speakers} data
   */
  async update(data: Speakers): Promise<Speakers | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${data.id}/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as Speakers);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a calendar instance.
   *  @param {String} instanceId
   */
  async delete(instanceId: String): Promise<Speakers | AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .delete(`/${instanceId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }

  async options(instanceId: String): Promise<AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .options(`/${instanceId}/`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
