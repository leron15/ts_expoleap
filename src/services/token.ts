import Vue from "vue";
import jwt_decode from "jwt-decode";

const TokenService = {
  data: null,

  //   async setToken(accessToken: string) {
  //     if (accessToken !== null) {
  //       localStorage.setItem("token", accessToken);
  //       this.data = await jwt_decode(this.getToken());
  //       console.log("TOKEN NOT NULL", this.data);
  //     } else {
  //       localStorage.setItem("token", this.getToken());
  //       this.data = await jwt_decode(this.getToken());
  //       console.log("TOKEN NULL", this.data);
  //     }
  //   },

  setToken(accessToken: string) {
    localStorage.setItem("token", accessToken);
  },

  getToken(): string {
    return localStorage.getItem("token") as string;
  },

  removeToken() {
    localStorage.removeItem("token");
    this.data = null;
  }

  //   decodeToken(): any {
  //     var decoded = jwt_decode(this.getToken());
  //     // console.log("DECODE:", decoded);
  //     this.data = decoded;
  //     return decoded as any;
  //   },

  //   async isTokenExpired() {
  //     if (this.getToken()) {
  //       await this.decodeToken().exp;
  //       var expire = this.decodeToken().exp;
  //       var date = new Date().valueOf();
  //       // console.log(!(date > expire));
  //     }
  //     return !(date > expire);
  //   }
};

const EventService = {
  //   Id: null,
  //   setEventId(data) {
  //     data == null
  //       ? localStorage.setItem("event", data)
  //       : (this.id = localStorage.getItem("event"));
  //   },
  //   getEventId() {
  //     return localStorage.getItem("event");
  //   }
};

export { TokenService, EventService };

export class JwtHelper {
  private urlBase64Decode(str: string): string {
    let output = str.replace(/-/g, "+").replace(/_/g, "/");
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += "==";
        break;
      case 3:
        output += "=";
        break;
      default:
        throw "Illegal base64url string!";
    }
    return decodeURIComponent((<any>window).escape(window.atob(output)));
  }

  public decodeToken(token: string = ""): object {
    if (token === null || token === "") {
      return { upn: "" };
    }
    const parts = token.split(".");
    if (parts.length !== 3) {
      throw new Error("JWT must have 3 parts");
    }
    const decoded = this.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error("Cannot decode the token");
    }
    return JSON.parse(decoded);
  }

  public get getToken(): string | boolean {
    return localStorage.getItem("access__token") || false;
  }
  public setToken(token: string): void {
    localStorage.setItem("access__token", token);
  }
  public removeToken(token: string): void {
    localStorage.removeItem("access__token");
  }
}
