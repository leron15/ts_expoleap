import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { Events, EventsPrototype } from "./interface";
import { JwtHelper } from "@/services/token";
import { formData } from "@/utils/formData";


export { EventService as service };
const token = new JwtHelper();

class EventService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/v1/events`;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.getToken}`;
    console.log("Initialized Events Service");
  };
  setEvent = async (id: string) => localStorage.setItem("event", id);

  apiKeys: any = Object.keys(EventsPrototype);

  // CREATE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Create an event instance.
   *  @param {Events} data
   */
  async create(data: Events): Promise<AxiosResponse | Events> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, formData.setForm(data))
        .then(response => {
          resolve(response.data as Events);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a event instance.
   */
  async fetch(): Promise<AxiosResponse[]> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`/`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a calendar instance.
   */
  async read(id: string): Promise<Events | any> {
    return await new Promise((resolve, reject) => {
      axios
        .get(`${id}`)
        .then(response => {
          return resolve(response.data as Events | any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // UPDATE
  /**
   * Update Calendar Occurrences
   *
   *  @description Create an eevnt instance.
   *  @param {Events} event
   */
  async update(data: Events): Promise<Events | any> {
    return await new Promise((resolve, reject) => {
      axios
        .patch(`/${data.id}`, formData.setForm(data))
        .then(response => {
          resolve(response.data as Events);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // DELETE
  /**
   * Fetch Calendar Occurrences
   *
   *  @description Delete a calendar instance.
   *  @param {String} eventId
   */
  async delete(eventId: String): Promise<Events | any> {
    return await new Promise((resolve, reject) => {
      axios
        .delete(`/${eventId}`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }

  async options(eventId: String): Promise<Events | any> {
    return await new Promise((resolve, reject) => {
      axios
        .options(`/${eventId}`)
        .then(response => {
          resolve(response.data as any);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
