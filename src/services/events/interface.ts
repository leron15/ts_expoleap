export interface Events {
  id?: string;
  organizer?: string;
  created?: Date | string;
  name?: string;
  description?: string;
  logo?: string;
  icon?: string;
  address?: string;
  locality?: string;
  state?: string;
  postal_code?: string;
  country?: string;
  start_date?: Date | string;
  start_time?: string;
  end_time?: string;
  end_date?: Date | string;
  website?: string;
  phone?: string;
  email?: string;
  facebook?: string;
  instagram?: string;
  twitter?: string;
  linkedin?: string;
  youtube?: string;
}

export const EventsPrototype: Events = {
  id: "",
  organizer: "",
  created: "",
  name: "",
  description: "",
  logo: "",
  icon: "",
  address: "",
  locality: "",
  state: "",
  postal_code: "",
  country: "",
  start_date: "",
  start_time: "",
  end_time: "",
  end_date: "",
  website: "",
  phone: "",
  email: "",
  facebook: "",
  instagram: "",
  twitter: "",
  linkedin: "",
  youtube: ""
};
