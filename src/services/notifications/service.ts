import { axios, AxiosResponse } from "@/plugins/axios";
import { errorCatch } from "../error";
import { JwtHelper } from "@/services/token";

export { NotificationsService as service };
const token = new JwtHelper();
class NotificationsService {
  init: any = () => {
    axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/fcm/`;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.getToken}`;
    console.log("Initialized Notifications Service");
  };

  public getEvent = () => localStorage.getItem("event");
  
  apiKeys: any = Object.keys([]);
  // setEvent = async (id: string) => localStorage.setItem("data", id);
  // POST
  /**
   * Push Notification
   *
   *  @description Push a notification to all devices.
   *  @param {String} title
   *  @param {String} message
   *  @param {String} event
   */
  async pushNotification(
    title: string,
    message: string,
    event: string
  ): Promise<AxiosResponse> {
    return await new Promise((resolve, reject) => {
      axios
        .post(`/`, {
          title: title,
          msg: message,
          data: '{"type":"1", "subtype":"1"}',
          event: event
        })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  // READ
  /**
   * Fetch Event Occurrences
   *
   *  @description Fetch a data instance.
   */
  async getDevices(): Promise<AxiosResponse> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`devices`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
  async getDevicesStats(): Promise<AxiosResponse> {
    console.log();
    return await new Promise((resolve, reject) => {
      axios
        .get(`device-stats/`)
        .then(response => {
          return resolve(response.data);
        })
        .catch(error => {
          return reject(errorCatch(error));
        });
    });
  }
}
