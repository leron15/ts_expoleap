import Vue from "vue";
import { VueAuthenticate } from "vue-authenticate";

const twitterAuth = new VueAuthenticate(Vue.prototype.$axios, {
  baseUrl: "http://alserver.ddns.net:8000",
  providers: {
    twitter: {
      url: "/auth/twitter/",
      event: localStorage.getItem("event")
    }
  }
});

const googleAuth = new VueAuthenticate(Vue.prototype.$axios, {
  baseUrl: "http://alserver.ddns.net:8000",
  providers: {
    google: {
      url: "/auth/google/",
      event: localStorage.getItem("event")
    }
  }
});

export { twitterAuth, googleAuth };
