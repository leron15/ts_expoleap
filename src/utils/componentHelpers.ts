// Must be called in Vue context
export function goTo(this: any, id: string) {
  this.$vuetify.goTo(id).then(() => {
    if (!id) return (document.location.hash = "");

    if (history.replaceState) {
      history.replaceState(null, null, id);
    } else {
      document.location.hash = id;
    }
  });
}

export function getAuthPage(type: string) {
  switch (type) {
    case "sign-in":
      return "auth-signin";
    case "sign-up":
      return "auth-signup";
    case "verify-email":
      return "auth-verify";
    case "forget-password":
      return "auth-forget-password";
    default:
      return "auth-signin";
  }
}
export function getPage(type: string = "page") {
  switch (type) {
    case "page":
      return "data-section";
    case "section":
      return "data-section";
    case "select":
      return "data-select";
    default:
      return type;
  }
}
export function getComponent(type: string = "list") {
  switch (type) {
    // case "page":
    //   return "data-section";
    // case "events-detail":
    //   return "events-detail";
    // case "accessibility":
    //   return "doc-accessibility";
    // case "api":
    //   return "doc-api";
    // case "checklist":
    //   return "doc-checklist";
    // case "example":
    //   return "doc-example";
    // case "examples":
    //   return "doc-examples";
    case "tasks":
      return "data-tasks";
    case "notify":
      return "data-notification";
    case "calendar":
      return "data-calendar";
    case "list":
      return "data-list";
    case "detail":
      return "data-detail";
    case "heading":
      return "base-heading";

    // case "img":
    //   return "doc-img";
    // case "text":
    //   return "doc-text";
    // case "markup":
    //   return "doc-markup";
    // case "markdown":
    //   return "base-markdown";
    // case "parameters":
    //   return "doc-parameters";
    // case "playground":
    //   return "doc-playground";
    // case "section":
    //   return "data-section";
    // case "supplemental":
    //   return "doc-supplemental";
    // case "tree":
    //   return "doc-tree";
    // case "up-next":
    //   return "doc-up-next";
    // case "usage":
    //   return "doc-usage";
    // case "usage-new":
    //   return "doc-usage-new";
    // case "locales":
    //   return "doc-locales";
    // case "variable-api":
    //   return "doc-variable-api";
    default:
      return type;
  }
}

export function parseLink(match: any, text: string, link: string) {
  let attrs = "";
  let icon = "";
  let linkClass = "v-markdown--link";

  // External link
  if (link.indexOf("http") > -1 || link.indexOf("mailto") > -1) {
    attrs = `target="_blank" rel="noopener"`;
    icon = "open-in-new";
    linkClass += " v-markdown--external";
    // Same page internal link
  } else if (link.charAt(0) === "#") {
    icon = "pound";
    linkClass += " v-markdown--same-internal";
    // Different page internal link
  } else {
    icon = "page-next";
    linkClass += " v-markdown--internal";
  }

  return `<a href="${link}" ${attrs} class="${linkClass}">${text}<i class="v-icon mdi mdi-${icon}"></i></a>`;
}

export async function waitForReadystate() {
  if (
    typeof document !== "undefined" &&
    typeof window !== "undefined" &&
    document.readyState !== "complete"
  ) {
    await new Promise(resolve => {
      const cb = () => {
        window.requestAnimationFrame(resolve);
        window.removeEventListener("load", cb);
      };

      window.addEventListener("load", cb);
    });
  }
}

export function genChip(item: any) {
  if (item.new) return "new";
  if (item.updated) return "updated";
  if (item.deprecated) return "deprecated";
  if (item.help) return "help";
}

export function getBranch() {
  const branch = window ? window.location.hostname.split(".")[0] : "master";

  return ["master", "dev", "next"].includes(branch) ? branch : "master";
}
