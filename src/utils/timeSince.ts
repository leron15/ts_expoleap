export function timeSince(date: Date) {
  if (date == null) {
    return null;
  }
  var day = new Date(date).valueOf() / 1000;
  var now = new Date().valueOf() / 1000;
  var seconds = Math.floor(now - day);

  var interval = Math.floor(seconds / 31557600);

  if (interval > 1) {
    return interval + " years ago";
  }
  interval = Math.floor(seconds / 2629800);
  if (interval > 1) {
    return interval + " months ago";
  }
  interval = Math.floor(seconds / 604800);
  if (interval > 1) {
    return interval + " weeks ago";
  }
  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return interval + " days ago";
  }
  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return interval + " hours ago";
  }
  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return interval + " minutes ago";
  }
  interval = Math.floor(seconds);
  if (interval > 1) {
    return Math.floor(seconds) + " seconds ago";
  }
  interval = Math.floor(-seconds / 604800);
  if (interval == 1) {
    interval = Math.floor(-seconds / 86400);
    if (interval > 7) {
      return null;
    } else {
      return "Due in 1 week";
    }
  } else if (interval > 1) {
    return null;
  }

  interval = Math.floor(-seconds / 86400);
  if (interval > 1) {
    return "Due in " + interval + " days";
  }
  interval = Math.floor(-seconds / 3600);
  if (interval > 1) {
    return "Due in " + interval + " hours";
  }
  interval = Math.floor(-seconds / 60);
  if (interval > 1) {
    return "Due in " + interval + " minutes";
  }
}
