const formData = {
  setForm: (payload: any) => {
    let data = new FormData();

    if (payload.file instanceof File) {
      data.append("file", payload.file);
    } else {
      payload.file = null;
    }
    if (payload.photo instanceof File) {
      data.append("photo", payload.photo);
    } else {
      payload.photo = null;
    }
    if (payload.logo instanceof File) {
      data.append("logo", payload.logo);
    } else {
      payload.logo = null;
    }
    if (payload.avatar instanceof File) {
      data.append("avatar", payload.avatar);
    } else {
      payload.avatar = null;
    }
    // data.append('id', payload.id)
    // data.append('address', payload.address)
    // data.append('country', payload.country)
    // data.append('description', payload.description)
    // data.append('content', payload.content)
    // data.append('prioity', payload.priority)
    // data.append('email', payload.email)
    // data.append('event', localStorage.getItem('event')) // temp solution
    // data.append('event', payload.event) // temp solution
    // data.append('name', payload.name)
    // data.append('author', payload.author)
    // data.append('title', payload.title)
    // data.append('locality', payload.locality)
    // data.append('state', payload.state)
    // data.append('postal_code', payload.postal_code)
    // data.append('phone', payload.phone)
    // data.append('country', payload.country)
    // data.append('start_date', payload.start_date)
    // data.append('start_time', payload.start_time)
    // data.append('end_date', payload.end_date)
    // data.append('end_time', payload.end_time)
    // data.append('website', payload.website)
    // data.append('url', payload.url)
    // data.append('facebook', payload.facebook)
    // data.append('instagram', payload.instagram)
    // data.append('linkedin', payload.linkedin)
    // data.append('twitter', payload.twitter)

    for (var key in payload) {
      if (payload.hasOwnProperty(key)) {
        if (
          key !== "logo" ||
          payload.logo instanceof File ||
          payload.file instanceof File ||
          payload.photo instanceof File
        ) {
          const value = payload[key];
          if (value != null) {
            // console.log('Append: key= ' + key + ', value= ' + value)
            data.append(key, value);
          }
        }
      }
    }
    return data;
  },
  getForm(data: any) {
    var form = this.setForm(data);
    return form;
  }
};
export { formData };
