export default {
  detail: {
    value: {
      type: String,
      default: () => "ok"
    }
    // api: {
    //   type: [Array, String],
    //   default: () => ""
    // }
  },
  list: {
    value: {
      type: String,
      default: () => "ok"
    },
    api: {
      type: [Array, String],
      default: () => ""
      // validate: validateApiCall,
    }
  }
};

const API_CALLS: string[] = [
  "events",
  "auth",
  "calendars",
  "speakers",
  "sponsors"
];
