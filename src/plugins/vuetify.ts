import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
  // ssr: Boolean(ssrContext),//TODO pass ssrContext into create vuetify
  theme: {
    options: {
      customProperties: true,
      minifyTheme: css => {
        return process.env.NODE_ENV === "production"
          ? css.replace(/[\s|\r\n|\r|\n]/g, "")
          : css;
      }
    },
    themes: {
      light: {
        primary: "#20a8d8",
        secondary: "#424242",
        accent: "#82B1FF",
        error: "#FF5252",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107"
      },
      dark: {
        primary: "#20a8d8",
        secondary: "#424242",
        accent: "#82B1FF",
        error: "#FF5252",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107"
      }
    }
  }
});
